FROM mcr.microsoft.com/dotnet/sdk:6.0.201-alpine3.15-arm64v8

RUN apk add make 

WORKDIR /build
COPY . .

RUN make publish
