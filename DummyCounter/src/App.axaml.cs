using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using DummyCounter.Views;
using DummyCounter.ViewModels;

namespace DummyCounter
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
          var window = new MainWindow
          {
              DataContext = new MainWindowViewModel(),
          };

          if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
          {
              desktop.MainWindow = window;
          }
          
          base.OnFrameworkInitializationCompleted();
        }
    }
}