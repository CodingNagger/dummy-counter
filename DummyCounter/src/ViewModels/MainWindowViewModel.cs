using System;
using ReactiveUI;
using System.Reactive;

namespace DummyCounter.ViewModels
{
    public class MainWindowViewModel : ReactiveObject
    {
      private int count;
      public ReactiveCommand<Unit, Unit> DoIncrement { get; }
      public ReactiveCommand<Unit, Unit> DoReset { get; }

      public MainWindowViewModel() {
        DoIncrement = ReactiveCommand.Create(Increment);
        DoReset = ReactiveCommand.Create(Reset);
      }

      public int Count
      {
          get => count;
          set => this.RaiseAndSetIfChanged(ref count, value);
      }

      void Increment()
      {
        Count++;
      }


      void Reset() {
        Count = 0;
      }
    }
}